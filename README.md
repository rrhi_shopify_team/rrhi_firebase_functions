[![GoRobinsons Logo](https://i2.wp.com/gorobinsons.ph/wp-content/uploads/2020/11/1_full_color_logo_2760x303.png)](https://gorobinsons.ph/)

Structured Implementation of GoR Firebase Functions made in [nodejs](https://nodejs.org/en/).

# RRHI Firebase Function Installation #

This is a [firebase-function](https://www.npmjs.com/package/firebase-tools) module that runs
in [Node.js](https://nodejs.org/en/) available through the [npm registry](https://www.npmjs.com/).

Before running, [download and install Node.js](https://nodejs.org/en/download/).
Node.js 0.10 or higher is required.

Once Installation is done. Go to [functions]() folder Run the line below to install the needed module.

```bash
$ npm install
```

### What is this repository for? ###

* Firebase FireStore Cloud Functions for GoRobinsons.
* Version 0.0.1

### Docs and Community ###
* [Website and Documentation](https://firebase.google.com/docs) - [[firebase-admin repo](https://github.com/firebase/firebase-tools), [firebase-tool repo](https://github.com/firebase/firebase-admin-node)]
* [Community](https://firebase.google.com/community)