/* eslint-disable camelcase */
/* eslint-disable @typescript-eslint/no-explicit-any*/

import * as functions from "firebase-functions";
import * as appSettings from "../../settings/appSettings.json";
import netOH from "../../services/onhand/netoh.service";

/* Function Name: usersOrderOnCreate
 * Description: Event triggered function on
 * when orders line_items is created */
const usersOrderOnCreate = functions.firestore.
    document("users/{uid}/orders/{id}").onCreate((snap, context) => {
      // Get Documents
      const document = snap.data();
      // Prepare json payload
      if (typeof document?.line_items != "undefined" && document?.line_items) {
        // Map line_items and map a new json
        appSettings.netOH.gotOptions.json = document.line_items.map(
            (item: {
                id: string,
                sku: number,
                quantity: number,
                fulfilled_quantity: number
            }) => {
              return {
                sku: item.sku,
                store: document.location_id,
                order_qty: item.quantity,
                delivered_qty: (typeof item.fulfilled_quantity != "undefined" &&
                    item.fulfilled_quantity) ?
                    item.fulfilled_quantity : 0,
                bu_id: document.business_unit_id,
                userid: context.params.uid,
                orderid: document.id,
                line_id: item.id,
                created_by: appSettings.netOH.createdBy,
              };
            });
        // Perform Net OH Post
        netOH.postNetOH(
            appSettings.netOH.netOHPostURLStg,
            appSettings.netOH.gotOptions)
            .then((response: any) => {
              functions.logger.info(
                  "List of Orders onCreate Net OH with (" +
                  response.statusCode +
                  ") Status"
              );
            })
            .catch((error: any) => {
              functions.logger.error(
                  "POST Net OH Error: ",
                  error
              );
            });
      }
    });
/* Function Name: usersOrderOnCreate
 * Description: Event triggered function on line_items
 * when orders line_items are updated */
const usersOrderOnUpdate = functions.firestore.
    document("users/{uid}/orders/{id}").
    onUpdate((change, context) => {
      // Get Documents
      const olddocument = change.before.data();
      const document = change.after.data();
      // Prepare json payload
      if (typeof document?.line_items != "undefined" && document?.line_items) {
        // Filter old and new line_items and map a new json
        appSettings.netOH.gotOptions.json = document.line_items.filter(
            (item: {
                id: string,
                sku: number,
                quantity: number,
                fulfilled_quantity: number
            }) => {
              return olddocument.line_items.filter(
                  (olditem: {
                        id: string,
                        sku: number,
                        quantity: number,
                        fulfilled_quantity: number
                  }) => olditem.id === item.id &&
                        olditem.sku === item.sku &&
                        olditem.fulfilled_quantity != item.fulfilled_quantity
              )[0];
            }).map(
            (item: {
                id: string,
                sku: number,
                quantity: number,
                fulfilled_quantity: number
            }) => {
              return {
                sku: item.sku,
                store: document.location_id,
                order_qty: item.quantity,
                delivered_qty: (typeof item.fulfilled_quantity != "undefined" &&
                    item.fulfilled_quantity) ?
                    item.fulfilled_quantity : 0,
                bu_id: document.business_unit_id,
                userid: context.params.uid,
                orderid: document.id,
                line_id: item.id,
                created_by: appSettings.netOH.createdBy,
              };
            });
        // Perform Net OH Post

        functions.logger.info(
            "appSettings.netOH.gotOptions.json:",
            appSettings.netOH.gotOptions.json
        );
        netOH.postNetOH(
            appSettings.netOH.netOHPostURLStg,
            appSettings.netOH.gotOptions)
            .then((response: any) => {
              functions.logger.info(
                  "List of Orders onCreate Net OH with (" +
                  response.statusCode +
                  ") Status"
              );
            })
            .catch((error: any) => {
              functions.logger.error(
                  "POST Net OH Error: ",
                  error
              );
            });
      }
    });
export {
  usersOrderOnCreate,
  usersOrderOnUpdate,
};
