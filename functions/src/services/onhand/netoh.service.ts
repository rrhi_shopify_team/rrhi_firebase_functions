/* eslint-disable @typescript-eslint/no-explicit-any*/

import got from "got";

const netOH = {
  postNetOH: (url: string, options: Record<string, any>):
    Record<string, any> => {
    return got.post(url, options);
  },
};

export default netOH;
